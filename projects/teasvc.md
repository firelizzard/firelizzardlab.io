---
title: Tea Service
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Tea Service [<code class="smaller-2">On GitLab</code>](https://gitlab.com/firelizzard/teasvc)

## A simple aid for service management

`teasvc` has two basic roles. One is to run servces, the other is to connect to a service. Service mode involves launching a service as a child process and then exporting an object on D-Bus. Connect mode involves using that exported object to connect back to the service-mode teasvc process. Connect mode primarily involves one of four activities: 1) connecting to the output of the child process; 2) connecting to the input and output of the child process; 3) sending a command to the child process; 4) or listing all launched service-mode teasvc processes.