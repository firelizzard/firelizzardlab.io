---
title: Go
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Go Code

## Go-Misc [<code class="smaller-2">On GitHub</code>](https://github.com/firelizzard18/go-misc)

Miscellaneous gocode used in other projects. Mainly synchronized/synchronization data structures implemented with channels. More of a proof of concept and a tool for understanding channels and synchronization than anything else.

## Goopt Fluent [<code class="smaller-2">On GitHub</code>](https://github.com/firelizzard18/goopt-fluent)

Goopt Fluent provides a [fluent interface](https://en.wikipedia.org/wiki/Fluent_interface) for configuring command line options, similar in capability to [getopt_long](https://linux.die.net/man/3/getopt_long) or [goopt](https://github.com/droundy/goopt).
