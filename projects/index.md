---
_layout: summary
title: Portfolio
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Portfolio

I've worked on many different things, so this is a subset. Currently, I'm employeed developing custom internal solutions delivered via on-premesis web and mobile applications. None of that work is public, so I can neither provide source nor talk about them in depth. Besides, the interesting part of those projects is the system architecture, which lends itself more to a blog than a porfolio.

In my free time, I work on a number of different things. My home router is from Ubiquiti's EdgeMax line, so it runs Ubiquiti's fork of VyOS, which is very configurable and very fun to play with. I occasionally work on [`teasvc`](/projects/teasvc.html), because managing services is a PITA and I wish it weren't. I took inspiration from [`onessg`](https://github.com/RyanZim/onessg) and made what amounts to a pipeline-based build system that happens to be useful for static site generation (https://gitlab.com/firelizzard/firesmith). I've got another project or two going on that aren't ready for the limelight.

Other than those, I included a few older, hopefully interesting projects written in [Objective-C](/projects/objc.html) and a couple in [Go](/projects/golang.html), plus a few [miscellaneous projects](/projects/misc.html).