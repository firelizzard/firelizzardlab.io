---
title: Objective-C
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Objective-C Projects

## CORM [<code class="smaller-2">On GitHub</code>](https://github.com/Firelizzard-Inventions/CORM)

The (Objective) C Object Relational Mapping (Framework) is an [object-relational mapping](https://en.wikipedia.org/wiki/Object-relational_mapping) framework for Objective-C.

## ORDA [<code class="smaller-2">On GitHub</code>](https://github.com/Firelizzard-Inventions/ORDA)

The Objective Relational Database Abstraction is a collection of Objective-C frameworks that aims to be an implementation agnostic tool for accessing and manipulating SQL-based and SQL-like relational databases. There is a base framework, ORDA, and multiple 'driver' frameworks that link against implementation specific C libraries.

ORDA's paradigm is loosely based on that of [JDBC](http://docs.oracle.com/javase/7/docs/technotes/guides/jdbc/index.html) in that drivers must be registered before use, a driver is associated with a [URL/URI scheme](https://en.wikipedia.org/wiki/URI_scheme), and connections are made from a central class that determines the driver to delegate to based on the URL scheme. All drivers must conform to the protocols that make up the 'specification'.

#### Implementations

Two implementations are provided:

  * [CocoaSQLite](https://github.com/Firelizzard-Inventions/CocoaSQLite)
  * [CocoaMySQL](https://github.com/Firelizzard-Inventions/CocoaMySQL)

## Type Extensions [<code class="smaller-2">On GitHub</code>](https://github.com/Firelizzard-Inventions/TypeExtensions)

Various Objective-C extensions to existing types.

#### Voodoo Warning

`NSObject (DeallocListener)` and `NSObject (zeroingWeakReferenceProxy)` contain some serious runtime voodoo.