---
title: Misc
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Miscellaneous Projects

## ShellSync [<code class="smaller-2">On GitHub</code>](https://github.com/firelizzard18/shellsync)
###### Previously DbxBin

ShellSync is a simple shell utility for synchronizing files via Dropbox.

## Virtual Bukkit [<code class="smaller-2">On GitHub</code>](https://github.com/firelizzard18/VirtualBukkit)

Virtual Bukkit is a simple tool that allows multiple minecraft servers (version 1.7+) to run in virtual-host style on a single outward-facing port. Minecraft version 1.7 introduced changes to the client-server protocol, including the addition of the eqiuvalent of an HTTP Host header. Thus, packets can be routed to the appropriate server.

## Entropy [<code class="smaller-2">On GitHub</code>](https://github.com/firelizzard18/Entropy)

Entropy is a JavaScript library that attepts to calculate the approximate entropy of a password, using word frequency lists and (simple) pattern recognition.