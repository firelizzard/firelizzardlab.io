This is my personal website.

## License

**You may do nothing with the content of this project other than view it.**

Any markdown files are copyright Ethan Reesor, all rights reserved.

Anything in `.site` is licensed under the [WTFPL](http://www.wtfpl.net/).

## Building

Don't. This is my personal website. If you want to use my generator, have fun.
But don't use it on any of the content files in this project.

## Bookmarks

[What Should You Include on Your Personal Resume Website?](https://business.tutsplus.com/articles/what-to-include-on-your-personal-resume-website--cms-28052)

[The Essential Components Of A Great Personal Website](https://collegeinfogeek.com/essential-components-of-personal-websites/)

[22 Ideas for Creating Great Content](https://www.themuse.com/advice/writers-block-22-ideas-for-creating-great-content)

[How to Tell People What You Do](https://www.themuse.com/advice/how-to-tell-people-what-you-doand-be-remembered)