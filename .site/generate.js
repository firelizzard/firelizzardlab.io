let firesmith = require('firesmith'),
    path = require('path'),
    fs = require('fs'),
    fse = require('fs-extra'),
    walk = require('klaw-sync'),
    rm = require('rimraf'),
    opts = require(path.join(__dirname, 'options.js'))

let sitesrc = path.join(__dirname, 'src')
let proj = path.resolve(__dirname, '..')
let sitedir = path.relative(proj, __dirname)

if (!fs.existsSync(sitesrc))
    fs.mkdirSync(sitesrc)

walk(sitesrc).forEach(({ path: p, stats }) => {
    let rel = path.relative(sitesrc, p)
    let src = path.join(proj, rel)

    if (fs.existsSync(src))
        return

    rm.sync(p)
})

walk(proj, {
    noRecurseOnFailedFilter: true,
    filter({ path: p, stats }) {
        let ext = path.extname(p)
        let base = path.basename(p)

        // skip dot directories
        if (!stats.isFile())
            return !base.startsWith('.')

        // skip readme
        if (base == 'README.md')
            return false

        // include defaults
        if (base == '_defaults.yml')
            return true

        // include markdown and html
        return ext == '.md' || ext == '.html'
    }
}).forEach(({ path: p, stats }) => {
    let rel = path.relative(proj, p)
    let dest = path.join(proj, sitedir, 'src', rel)

    if (stats.isFile()) {
        console.log('cp %s', rel)
        fse.copyFileSync(p, dest)
        return
    }

    if (fs.existsSync(dest))
        return

    console.log('mkdir %s', rel)
    fs.mkdirSync(dest)
})

firesmith(opts.config, opts.alterations).catch((err) => {
    console.error(err, err.stack)
    process.exit(1)
})