let firesmith = require('firesmith'),
    express = require('express'),
    watch = require('node-watch'),
    fs = require('fs'),
    fse = require('fs-extra'),
    path = require('path'),
    rm = require('rimraf'),
    opts = require(path.join(__dirname, 'options.js'))

require(path.join(__dirname, 'generate.js'))

watch(['src', 'layouts'].map(x => path.join(__dirname, x)), { recursive: true }, function(evt, p) {
    console.log('%s changed.', path.relative(__dirname, p))
    firesmith(opts.config, opts.alterations).catch(console.log)
})

let proj = path.resolve(__dirname, '..')
let sitedir = path.relative(proj, __dirname)

watch(path.join(__dirname, '..'), { recursive: true }, (evt, p) => {
    for (let dir of p.split(path.sep))
        if (dir.startsWith('.'))
            return

    let rel = path.relative(proj, p)
    let dest = path.join(proj, sitedir, 'src', rel)

    if (!fs.existsSync(p)) {
        if (fs.existsSync(dest))
            rm.sync(dest)
        return
    }

    if (fs.statSync(p).isDirectory() && !fs.existsSync(dest)) {
        fs.mkdirSync(dest)
        return
    }

    let ext = path.extname(p)
    let base = path.basename(p)
    if (ext != '.md' && ext != '.html' && base != '_defaults.yml' || base == 'README.md')
        return

    fse.copyFileSync(p, dest)
})

let app = express();
['dist', 'www'].map(x => app.use(express.static(path.join(__dirname, x))))

app.listen(1337)