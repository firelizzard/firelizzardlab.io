This is a website generator. It takes markdown files from the main directory
and any subdirectories not starting with `.` and uses `firesmith` to generate a
website.

## License

Do What The Fuck You Want. This file and all other files in this directory and
its subdirectories are licensed under the [WTFPL](http://www.wtfpl.net/). See
`LICENSE` in this directory for details.

## Build

Do not use this to build my website. If you are using for your own:

#### `generate.js` / `npm run-script generate`

Copies all markdown files and `_default.yml` files into `.site/src` and builds
the website with `firesmith`.

#### `serve.js` / `npm start`

Executes `generate.js` and then watches the content files, `www` files, and
layout files. If any of those files changes, rebuilds the website. **Buggy**.
