Object.defineProperties(Object.prototype, {
    forEach: {
        value: function (fn) {
            return Object.keys(this).forEach(key => fn(key, this[key]))
        }
    }
})

global.dirname = function dirname(path) {
    return path.split('/').slice(0, -1).join('/')
}

global.basename = function basename(path) {
    return path.split('/').slice(-1)[0]
}

global.navpath = function navpath(path) {
    return /^\w+:\/\//.test(path) ? path : `/${path}.html`
}

global.tabpath = function tabpath(base, path) {
    return /^\w+:\/\//.test(path) ? path : `/${dirname(base)}/${path}.html`
}

global.linktext = function (text) {
    return text.replace('[x]', '<img style="opacity: 0.7" src="https://upload.wikimedia.org/wikipedia/commons/d/d9/VisualEditor_-_Icon_-_External-link.svg" alt="external" />')
}

global.target = function (text) {
    return / \[x\]$/.test(text) ? 'target="_blank"' : ''
}