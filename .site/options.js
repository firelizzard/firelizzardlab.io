let path = require('path'),
    showdown = require('showdown'),
    _

require('./globals')

let Recipe = require('firesmith/pipe/recipe')
let Content = require('firesmith/lib/content')

let conv = new showdown.Converter()

let config = {
    src: path.join(__dirname, 'src'),
    dist: path.join(__dirname, 'dist'),
    layouts: path.join(__dirname, 'layouts'),

    require
}

Content.Pipelines.RenderPipeline.register({
    renderMarkdownWithShowdown() {
        return this.renderMarkdownWith(x => conv.makeHtml(x))
    }
})

let syncpoints = []
module.exports = {
    config,
    alterations: { content: { render: {
        excludeDrafts: new Recipe.Sequence([
            'excludeDrafts',
            pl => pl.useDelegate(async (rsc, next) => {
                console.log('processing %s', rsc.name)
                return await next(rsc)
            })
        ]),
        render: { markdown: 'renderMarkdownWithShowdown' }
    } } }
}