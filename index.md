---
_layout: summary
title: Welcome
---

# Welcome

Hi. I'm Ethan Reesor and this is my website.

I'm a programmer. I've been writing code for 15 years and I have strong opinions. Currently, I'm most comfortable with JavaScript, C#, and Go, though I've worked with many, many other langauges, especially Objective-C. Don't talk to me about Java. I'd rather it didn't exist.

I've worked on a wide variety of projects, the only common denominator being programming. I'm currently employeed developing custom internal solutions delivered via on-premesis web and mobile applications. I tend more towards server-side work and system design than user facing development, but I do some of everything.

If you want to know more about me, look [here](/me). If you're curious what projects I've worked on, I have a [portfolio](/projects) that features a selection of my work.

## Want to play a game?

[4D Tic Tac Toe](/t4.html)