---
title: Contact
---

# Contact Me

The best way to reach me is [via email](mailto:ethan.reesor@gmail.com). You can also find me on [LinkedIn](https://www.linkedin.com/in/ethanreesor/).

For personal communications, contact me [via email](mailto:firelizzard@gmail.com). I'm not much of one for social media.

To stay up to date, follow me on [GitHub](https://github.com/firelizzard18) and [GitLab](https://gitlab.com/firelizzard).