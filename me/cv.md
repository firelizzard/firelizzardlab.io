---
title: CV
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Curriculum Vitae

## Education

**BS in Computer Science, May 2014 — Capitol College, Laurel, MD**

**BS in Electrical Engineering, May 2014 — Capitol College, Laurel, MD**

## Professional Experience

**Database & Systems Developer**, 2014-2015, 2016-Present — Millennium Space Systems, El Segundo, CA

  * Designed and developed an inventory management system in support of satellite development and manufacturing operations
  * Designed and developed a supplier management system in support of supply chain operations
  * Lead a team of developers in designing and developing a software license and inventory management system
  * Spearheaded expanded utilization of advanced version control and development operations systems
  * Spearheaded broad adoption of development operations best practices
  * Designed and developed numerous smaller solutions to workflow inefficiencies
  * Implemented a SAML SSO Identity Provider
  * Designed and implemented a framework for rapid development of service-based web applications

**Flight Systems Development Engineer**, 2016 — Millennium Space Systems, El Segundo, CA

  * Revised control logic for satellite radio subsystems
  * Reworked satellite subsystem requirements with use-case methodologies
  * Consulted on top-level satellite system requirements
  * Developed a requirements and test coverage reporting and analysis tool

## Projects

**ParticleDB**

  * Designed and developed a database engine based on a novel approach to data structure and storage