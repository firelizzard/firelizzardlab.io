---
title: Skills
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

# Skills

## Programming and Software Development

I am fluent in procedural and object-oriented programming (including class-based
and prototype-based OOP), and I am comfortable with functional programming.

I know many programming languages. I find it more useful to discuss proficiency
in terms of paradigms rather than languages, as I find languages rather easy to
learn, provided their primary paradigms are ones I know. That being said, I am
currently fluent in Go, C#, and JavaScript. I am very experienced with C,
Objective-C, and Java, though I have not worked with them recently and I hate
Java for mostly personal reasons. I am experienced with assembly (mostly x86),
Python, Bash, Mathematica, MatLab, Scala, and PHP. I am comfortable with Awk,
C++, Ruby, and Perl. On the non-programming side, I am fluent in SQL, CSS, and
HTML, and I am experienced with VHDL and Verilog.

I have a broad range of skills and I like challenges. Professionally, my focus
is on system design and architecture. I love approaching a daunting problem and
decomposing it into manageable components. I also very much enjoy cracking
seemingly inexplicable bugs, be they mine or those of someone else. I am
absolutely fascinated by the design of languages and language runtimes. I plan
on pursing a PhD in Computer Science for the purpose of realizing my plans for a
cross-platform language and runtime superior to Java or .NET.

## System and Network Administration

I am very familiar with Linux, and I have a working knowledge of system
administration and network administration. I know enough that I don't need to
rely on a professional for daily work, but I am not a professional system or
network administrator, no do I seek to be one.