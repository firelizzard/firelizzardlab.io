---
title: Testimonials
---

# Testimonials

> Ethan is one of those rare individuals who could break down problems (be they philosophical, linguistic, mechanical, whatever) and somehow instinctively know how they work. He has a great work ethic and any teacher should count themselves lucky to have him as a student. I know that I do.
- George Richard Odum, Director of Advanced Academics at Hays CISD