---
_layout: summary
title: About
license: Copyright © 2018 Ethan Reesor, All Rights Reserved
---

## About Me

I am a programmer. I am driven by the joy of making a difference in people's
lives. I love challenges, and I love learning new things. I am voraciously
curious about (almost) anything I don't yet understand.

If you are interested in what I've done, check out [my portfolio](/projects) or
[my GitLab account](https://gitlab.com/firelizzard). If you are interested in my
[work history](/me/cv.html) or [skills](/me/skills.html), check those out. If
you want to know what people have said about me, you can find that
[here](/me/testimonials.html).